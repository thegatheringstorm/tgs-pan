module.exports = {
    'ACCESS_KEY': process.env.ACCESS_KEY,  // https://portal.qiniu.com/user/key
    'SECRET_KEY': process.env.SECRET_KEY,
    'Bucket_Name': process.env.Bucket_Name,
    'Port': process.env.HTTP_PORT || 1234,
    'Uptoken_Url': 'uptoken',
    'Domain': process.env.Domain // bucket domain eg:http://qiniu-plupload.qiniudn.com/
};
