FROM node:6.10

RUN mkdir -p /app
WORKDIR /app

COPY package.json /app
RUN npm install -g cnpm --registry=https://registry.npm.taobao.org
RUN cnpm install

COPY . /app
EXPOSE 1234

CMD [ "node", "server/server.js" ]
